//
//  StocksAppSwiftUIApp.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 21/12/20.
//

import SwiftUI

@main
struct StocksAppSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
