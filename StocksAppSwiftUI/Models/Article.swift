//
//  Article.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 22/12/20.
//

import Foundation

struct Article: Decodable{
    
    var title       : String
    var imageURL    : String
    var publication : String
}
