//
//  Stock.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 21/12/20.
//

import Foundation
struct Stock: Decodable{
    
    let symbol      : String
    let description : String
    let change      : String
    let price       : Double
}
