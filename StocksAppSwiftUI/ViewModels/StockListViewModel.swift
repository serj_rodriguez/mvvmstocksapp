//
//  StockListViewModel.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 21/12/20.
//

import Foundation
import SwiftUI
class StockListViewModel: ObservableObject{
    @Published var searchTerm : String = ""
    @Published var stocks : [StockViewModel] = [StockViewModel]()
    @Published var articles: [NewsArcticleViewModel] = [NewsArcticleViewModel]()
    @Published var dragOffset : CGSize = CGSize(width: 0, height: 650)
    public func load(){
        fetchNews()
        fetchStocks()
    }
    private func fetchNews(){
        WebService().getTopNews{ articles in
            if let articles = articles{
                DispatchQueue.main.async {
                    self.articles = articles.map(NewsArcticleViewModel.init)
                }
            }
        }
    }
    private func fetchStocks(){
        WebService().getStocks{ stocks in
            if let stocks = stocks{
                DispatchQueue.main.async {
                    self.stocks = stocks.map(StockViewModel.init)
                }
            }
        }
    }
}
