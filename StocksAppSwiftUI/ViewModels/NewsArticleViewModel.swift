//
//  NewsArticleViewModel.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 22/12/20.
//

import Foundation

struct NewsArcticleViewModel{
    let article : Article
    
    var imageURL: String{
        return self.article.imageURL
    }
    var title: String{
        return self.article.title
    }
    var publication:String{
        return self.article.publication.uppercased()
    }
}
