//
//  NewsArticleView.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 22/12/20.
//

import SwiftUI
import URLImage
struct NewsArticleView: View {
    
    let newsArticles : [NewsArcticleViewModel]
    let onDragBegin : (DragGesture.Value) -> Void
    let onDragEnd : (DragGesture.Value) -> Void
    
    var body: some View {
        
        let screenSize = UIScreen.main.bounds.size
        
        return VStack(alignment: .leading){
            HStack{
                VStack(alignment: .leading){
                    Text("Top news")
                        .foregroundColor(Color.white)
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding(2)
                    Text("From News")
                        .foregroundColor(Color.gray)
                        .font(.body)
                        .fontWeight(.bold)
                        .padding(2)
                }
                Spacer()
            }.padding().contentShape(Rectangle())
            .gesture(DragGesture()
                        .onChanged(self.onDragBegin)
                        .onEnded(self.onDragEnd))
            ScrollView{
                VStack{
                    ForEach(self.newsArticles, id: \.title){ article in
                        HStack{
                            VStack(alignment: .leading){
                                Text(article.publication)
                                    .foregroundColor(Color.white)
                                    .font(.custom("Arial", size: 22))
                                    .fontWeight(.bold)
                                Text(article.title)
                                    .foregroundColor(Color.white)
                                    .font(.custom("Arial", size: 22))
                            }
                            Spacer()
                            URLImage(url: URL(string: article.imageURL)!, content:{ image in
                                        image.resizable()})
                                .frame(width: 100, height: 100)
                        }
                    }
                }.frame(maxWidth: .infinity)
            }
        }.frame(width: screenSize.width, height: screenSize.height)
        .background(Color(red: 27/255, green: 28/255, blue: 30/255))
        .cornerRadius(20.0)
    }
}

struct NewsArticleView_Previews: PreviewProvider {
    static var previews: some View {
        let article = Article(title: "News Title", imageURL: "https://www.elsevier.com/__data/assets/image/0009/899451/RA-review-articles-banner-1200-x-600.jpg", publication: "The Wallstreet Journal")
        NewsArticleView(newsArticles: [NewsArcticleViewModel(article: article)], onDragBegin: { _ in }, onDragEnd: { _ in })
    }
}
