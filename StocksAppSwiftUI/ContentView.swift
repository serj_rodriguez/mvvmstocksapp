//
//  ContentView.swift
//  StocksAppSwiftUI
//
//  Created by SERGIO ANDRES RODRIGUEZ CASTILLO on 21/12/20.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var stockListViewModel = StockListViewModel()
    
    init(){
        UINavigationBar.appearance().backgroundColor = UIColor.black
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UITableView.appearance().backgroundColor = UIColor.black
        UITableViewCell.appearance().backgroundColor = UIColor.black
        stockListViewModel.load()
    }
    
    var body: some View {
        
        let filteredStocks = self.stockListViewModel.searchTerm.isEmpty ? self.stockListViewModel.stocks : self.stockListViewModel.stocks.filter{ $0.symbol.starts(with: self.stockListViewModel.searchTerm)}
        return NavigationView{
            ZStack(alignment: .leading){
                Color.black
                Text("December 21, 2021")
                    .font(.custom("Arial", size: 32))
                    .fontWeight(.bold)
                    .foregroundColor(Color.gray)
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))
                    .offset(y: -400)
                
                SearchView(searchTerm: self.$stockListViewModel.searchTerm)
                    .offset(y: -300)
                StockListView(stocks: filteredStocks)
                    .offset(y: 100)
                
                NewsArticleView(newsArticles: self.stockListViewModel.articles,
                                onDragBegin:{ value in
                                    self.stockListViewModel.dragOffset = value.translation
                                },
                                onDragEnd: { value in
                                    if value.translation.height < 0{
                                        self.stockListViewModel.dragOffset = CGSize(width: 0, height: 100)
                                    }else{
                                        self.stockListViewModel.dragOffset = CGSize(width: 0, height: 650)
                                    }
                                })
                    .animation(.spring())
                    .offset(y: self.stockListViewModel.dragOffset.height)
                
            }
                .navigationBarTitle("Stocks")
        }.edgesIgnoringSafeArea(Edge.Set(.bottom))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
